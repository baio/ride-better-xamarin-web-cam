﻿using System;
using Android.App;
using Android.Widget;
using Android.OS;
using Android.Hardware;
using System.Threading;
using System.Threading.Tasks;
using Org.Json;

namespace ride_better_xamarin_web_cam
{
    [Activity(Label = "ride_better_xamarin_webcam", MainLauncher = true, Icon = "@drawable/icon", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class MainActivity : Activity
    {
        
        //internal const string UploadUrl = "http://172.30.99.102:8007/snapshots/1936";
        internal const string UploadUrl = "http://130.211.72.105:8000/snapshots/1936/1";

        PowerManager.WakeLock _wl;
        int _interval = 60;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            // Get our button from the layout resource,
            // and attach an event to it
            Button button = FindViewById<Button>(Resource.Id.MyButton);

            button.Click += Button_Click;

            PowerManager pm = (PowerManager)GetSystemService(PowerService);
            _wl = pm.NewWakeLock(WakeLockFlags.Partial, "My Tag");
            _wl.Acquire();

        }

        protected void onDestroy()
        {
            _wl.Release();
        }


        private void Button_Click(object sender, EventArgs e)
        {
            ((Button)sender).Text = "Started";
            ((Button)sender).Enabled = false;
            ((Button)sender).Click -= Button_Click;

            Task.Factory.StartNew(() => {
                while (true)
                {
                    var task = TakePicture();
                    task.Wait(6000);
                    System.Console.WriteLine("Done 1");
                    if (task.IsCompleted)
                    {
                        System.Console.WriteLine("Done 2 " + task.Result.Length);

                        task = UploadPhoto(task.Result);

                        task.Wait(6000);

                        System.Console.WriteLine("Done 3");

                        if (task.IsCompleted)
                        {
                            System.Console.WriteLine("Done 4");

                            var timeout = ParseResonse(task.Result);

                            System.Console.WriteLine("Done 5" + timeout);

                            if (timeout > 1 && timeout <= 60 * 60 * 6)
                            {
                                _interval = timeout;
                            }
                        }
                    }

                    System.Threading.Thread.Sleep(_interval * 1000);
                }
            });
        }



        private int ParseResonse(byte[] Response)
        {
            var res = System.Text.Encoding.UTF8.GetString(Response);

            System.Console.WriteLine("res " + res);

            JSONObject jObject = new JSONObject(res);
            if (jObject.Has("config") && (jObject.GetJSONObject("config")).Has("interval"))
            {
                return (jObject.GetJSONObject("config")).GetInt("interval");
            }
            else
            {
                return -1;
            }

        }

        private Task<byte[]> TakePicture()
        {
            return Task.Factory.StartNew(() =>
            {
                ManualResetEvent mre = new ManualResetEvent(false);
                //var preview = new PerviewCallback(mre);
                var picCallback = new PictureCallback(mre);
                var camera = Camera.Open();                
                //camera.SetOneShotPreviewCallback(preview);
                camera.StartPreview();
                camera.TakePicture(new ShutterictureCallback(), new RawPictureCallback(), picCallback);
                mre.WaitOne();
                mre.Dispose();

                camera.StopPreview();
                camera.Release();
                camera.Dispose();
                camera = null;

                return picCallback._data;
            });
        }


        public Task<byte[]> UploadPhoto(byte[] photoBytes)
        {
            using (System.Net.WebClient client = new System.Net.WebClient())
            {
                client.Headers.Add("Content-Type", "binary/octet-stream");
                return client.UploadDataTaskAsync(new Uri(MainActivity.UploadUrl), "POST", photoBytes);
            }
        }

    }

    
    class ShutterictureCallback : Java.Lang.Object, Camera.IShutterCallback
    {
        public void OnShutter()
        {
        }
    }

    class RawPictureCallback : Java.Lang.Object, Camera.IPictureCallback
    {
        public void OnPictureTaken(byte[] data, Camera camera)
        {
        }
    }
    


    class PictureCallback : Java.Lang.Object, Camera.IPictureCallback
    {
        private readonly ManualResetEvent _mre;
        internal byte[] _data;


        public PictureCallback(ManualResetEvent mre)
        {
            _mre = mre;
        }


        public void OnPictureTaken(byte[] data, Camera camera)
        {
            _data = data;
            _mre.Set();
        }

    }
}

